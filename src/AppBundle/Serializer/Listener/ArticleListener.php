<?php

namespace AppBundle\Serializer\Listener;

use AppBundle\Entity\Article;

use JMS\Serializer\EventDispatcher\Events;
use JMS\Serializer\EventDispatcher\EventSubscriberInterface;
use JMS\Serializer\EventDispatcher\ObjectEvent;

class ArticleListener implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {

        return [
            [
                'event' => Events::POST_SERIALIZE,
                'format' => 'json',
                'method' => 'onPostSerialize',

            ],

        ];
    }

    public function onPostSerialize(ObjectEvent $event)
    {

        $object = $event->getObject();

        if($object instanceof Article ){

            $date = new \Datetime();

            $event->getVisitor()->setData('delivered_at', $date->format('l jS \of F Y h:i:s A'));

        }


    }

}