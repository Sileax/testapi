<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Author;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;

use Symfony\Component\Routing\Annotation\Route;

use JMS\Serializer\SerializationContext;

/**
 * @Rest\Route("/api")
 */

class AuthorController extends Controller
{
    /**
     * @Route("/authors/{id}",
     *     name="author_show",
     *     methods={"GET"},
     *     requirements={"id"="\d+"})
     */
    public function showAction(Author $author)
    {

        $data = $this->get('jms_serializer')->serialize($author, 'json', SerializationContext::create()->setGroups(array('detail')));

        $response = new Response($data);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * @Route("/authors", name="author_create", methods={"POST"})
     */
    public function createAction(Request $request)
    {
        $data = $request->getContent();
        $author = $this->get('jms_serializer')->deserialize($data, 'AppBundle\Entity\Author', 'json');

        $this->get('validator')->validate($author);

        $em = $this->getDoctrine()->getManager();
        $em->persist($author);
        $em->flush();

        return new Response('', Response::HTTP_CREATED);
    }


    /**
     * @Route("/authors", name="author_list", methods={"GET"})
     */
    public function listAction()
    {

        $em = $this->getDoctrine()->getManager();

        $authors = $em->getRepository('AppBundle:Author')->findAll();

        $serializer = $this->get('jms_serializer');

        $data = $serializer->serialize($authors, 'json', SerializationContext::create()->setGroups(array('list')));

        $response = new Response($data);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }
}
