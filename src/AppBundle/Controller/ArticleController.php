<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Article;

use AppBundle\Form\ArticleType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * @Rest\Route("/api")
 */
class ArticleController extends FOSRestController
{
    /**
     * @Rest\Get(
     *     path = "/articles/{id}",
     *     name = "app_article_show",
     *     requirements = {"id"="\d+"}
     * )
     * @Rest\View
     */
    public function showAction(Article $article)
    {

        return $article;
    }


    /**
     * @Rest\Get(
     *     path = "/articles",
     *     name = "app_article_list",
     *     requirements = {"id"="\d+"}
     * )
     * @Rest\View
     * @Rest\QueryParam(
     *     name="order",
     *     requirements="asc|desc",
     *     default="asc",
     *     description="Sort order (asc or desc)"
     * )
     */
    public function listAction($order)
    {
        $em = $this->getDoctrine()->getManager();

        $articles = $em->getRepository('AppBundle:Article')->myFindAll($order);

        return $articles;
    }



    /**
     * @Rest\Post(
     *     path = "/articles",
     *     name = "app_article_new"
     * )
     * @Rest\View
     */
    public function createAction(Request $request)
    {

        $data = $this->get('jms_serializer')->deserialize($request->getContent(), 'array', 'json');

        $article = new Article();

        $form = $this->get('form.factory')->create(ArticleType::class, $article);

        $form->submit($data);

        $em = $this->getDoctrine()->getManager();

        $em->persist($article);

        $em->flush();

        return $this->view(
            $article,
            Response::HTTP_CREATED,
            [
                'Location' => $this->generateUrl('app_article_show', ['id' => $article->getId(), UrlGeneratorInterface::ABSOLUTE_URL])
            ]
        );
    }


}